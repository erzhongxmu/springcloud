package com.zhaodui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuulBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulBookApplication.class, args);
	}

}
