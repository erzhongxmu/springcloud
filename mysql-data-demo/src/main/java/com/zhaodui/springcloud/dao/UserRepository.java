package com.zhaodui.springcloud.dao;

import com.zhaodui.springcloud.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @ClassName: UserRepository
 * @Description:  用户仓库
 * @Author: zhaodui
 * @Date: 2019/4/1
 * @blog: https://www.imooc.com/u/1323320/articles
 **/
public interface UserRepository extends CrudRepository<User, Integer> {
}
