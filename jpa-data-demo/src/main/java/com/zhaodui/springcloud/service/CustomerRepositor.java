package com.zhaodui.springcloud.service;

import com.zhaodui.springcloud.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @ClassName: CustomerRepositor
 * @Description:
 * @Author: zhaodui
 * @Date: 2019/4/2
 * @blog: https://www.imooc.com/u/1323320/articles
 **/
public interface CustomerRepositor extends CrudRepository<Customer, Long> {

    List<Customer> findByName(String name);


}
