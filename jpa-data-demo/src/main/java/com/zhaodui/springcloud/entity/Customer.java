package com.zhaodui.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @ClassName: Customer
 * @Description:
 * @Author: zhaodui
 * @Date: 2019/4/2
 * @blog: https://www.imooc.com/u/1323320/articles
 **/
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String content;

    private Date createTime;
}
